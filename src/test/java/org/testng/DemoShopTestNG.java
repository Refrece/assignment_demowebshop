package org.testng;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DemoShopTestNG {
	private WebDriver driver;
    @BeforeClass
    public void setup() {
        driver = new ChromeDriver();
        driver.get("https://demowebshop.tricentis.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    }
    @AfterClass
    public void closedown() {
        driver.quit();
    }
    @Test(priority = 1)
    public void loginTest() throws InterruptedException {
        WebElement login = driver.findElement(By.linkText("Log in"));
        login.click();
        WebElement email = driver.findElement(By.id("Email"));
        email.sendKeys("manzmehadi1@gmail.com");
        WebElement password = driver.findElement(By.id("Password"));
        password.sendKeys("Mehek@110");
        WebElement loginButton = driver.findElement(By.cssSelector("input[value='Log in']"));
        loginButton.click();
        Thread.sleep(2000);
        String title = driver.getTitle();
        System.out.println(title);
    }
    @Test(priority = 2)
    public void booksTest() throws InterruptedException {
        driver.findElement(By.xpath("(//*[contains(@href,'/books')])[1]")).click();
        Thread.sleep(2000);
        Select s = new Select(driver.findElement(By.id("products-orderby")));
        s.selectByVisibleText("Price: High to Low");
        driver.findElement(By.xpath("(//*[contains(@value,'Add to cart')])[1]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("(//*[contains(@value,'Add to cart')])[2]")).click();
    }
    @Test(priority = 3)
    public void electronicsTest() throws InterruptedException {
        WebElement ele = driver.findElement(By.xpath("(//*[contains(@href,'/electronics')])[1]"));
        Actions action = new Actions(driver);
        action.moveToElement(ele).perform();
        Thread.sleep(2000);
        driver.findElement(By.xpath("(//*[contains(@href,'/cell-phones')])[1]")).click();
        driver.findElement(By.xpath("(//input[@type='button'])[3]")).click();
        Thread.sleep(2000);
        WebElement count = driver.findElement(By.xpath("(//a[@class='ico-cart'])[1]"));
        System.out.println(count.getText());
        Thread.sleep(2000);
    }
    @Test(priority = 4)
    public void giftCardsTest() throws InterruptedException {
        driver.findElement(By.xpath("(//*[contains(@href,'/gift-cards')])[1]")).click();
        Select s1 = new Select(driver.findElement(By.id("products-pagesize")));
        s1.selectByVisibleText("4");
        Thread.sleep(2000);
        WebElement we = driver.findElement(By.xpath("(//div[@class='product-item'])[1]"));
        System.out.println(we.getText());
    }
    @Test(priority = 5)
    public void logoutTest() {
        driver.findElement(By.linkText("Log out")).click();
        if (driver.findElement(By.linkText("Log in")).isDisplayed()) {
            System.out.println("login is displayed");
        } else {
            System.out.println("login is not displayed");
        }
    }
}
