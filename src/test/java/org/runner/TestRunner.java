package org.runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/features/DemoWebShop.feature",glue="org.step",
plugin = {"html:target/cucumber-reports"},monochrome = true)
public class TestRunner {

}
