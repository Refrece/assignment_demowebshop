package org.step;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DemoShopSteps {
	static WebDriver driver;
	@Given("user have to enter DemoWebShop application through chrome browser")
	public void user_have_to_enter_demo_web_shop_application_through_chrome_browser() {
    driver = new ChromeDriver();
    driver.get("https://demowebshop.tricentis.com/");
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	}
	@When("click on the login button")
	public void click_on_the_login_button() {
		driver.findElement(By.linkText("Log in")).click();
	}

	@When("user have to enter valid {string} and valid {string}")
	public void user_have_to_enter_valid_and_valid(String email, String password) {
		driver.findElement(By.id("Email")).sendKeys(email);
		driver.findElement(By.id("Password")).sendKeys(password);
	}

	@Then("user have to click login button")
	public void user_have_to_click_login_button() throws InterruptedException {
		driver.findElement(By.cssSelector("input[value='Log in']")).click();
		Thread.sleep(2000);
	}

	@Then("user have to get title of the home page")
	public void user_have_to_get_title_of_the_home_page() {
        String title = driver.getTitle();
        System.out.println(title);
	}

	@Given("user have to click book category")
	public void user_have_to_click_book_category() throws InterruptedException {
        driver.findElement(By.xpath("(//*[contains(@href,'/books')])[1]")).click();
        Thread.sleep(2000);
	}

	@When("user have to select price category")
	public void user_have_to_select_price_category() {
		Select s = new Select(driver.findElement(By.id("products-orderby")));
        s.selectByVisibleText("Price: High to Low");
	}

	@When("user have to  sort price high to low")
	public void user_have_to_sort_price_high_to_low() throws InterruptedException {
		Thread.sleep(2000);
	}

	@Then("user have to Add books in add to cart")
	public void user_have_to_add_books_in_add_to_cart() throws InterruptedException {
        driver.findElement(By.xpath("(//*[contains(@value,'Add to cart')])[1]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("(//*[contains(@value,'Add to cart')])[2]")).click();
	}

	@Then("user have to click Electronic category")
	public void user_have_to_click_electronic_category() throws InterruptedException {
        Thread.sleep(2000);
	}

	@Then("user have to select cellphone")
	public void user_have_to_select_cellphone() {
		WebElement move = driver.findElement(By.xpath("(//*[contains(@href,'/electronics')])[1]"));
        Actions action = new Actions(driver);
        action.moveToElement(move).perform();
	}

	@Then("user have to add any product in add to cart")
	public void user_have_to_add_any_product_in_add_to_cart() throws InterruptedException {
        driver.findElement(By.xpath("(//*[contains(@href,'/cell-phones')])[1]")).click();
        driver.findElement(By.xpath("(//input[@type='button'])[3]")).click();
        Thread.sleep(2000);
	}

	@Then("user have to display count of items present in shopping cart")
	public void user_have_to_display_count_of_items_present_in_shopping_cart() throws InterruptedException {
        WebElement count = driver.findElement(By.xpath("(//a[@class='ico-cart'])[1]"));
        System.out.println(count.getText());
        Thread.sleep(2000);
	}

	@Given("user have to select gift card category")
	public void user_have_to_select_gift_card_category() throws InterruptedException {
        driver.findElement(By.xpath("(//*[contains(@href,'/gift-cards')])[1]")).click();
        Select s1 = new Select(driver.findElement(By.id("products-pagesize")));
        s1.selectByVisibleText("4");
        Thread.sleep(2000);
	}

	@When("user have to capture any one of the giftcard name and price")
	public void user_have_to_capture_any_one_of_the_giftcard_name_and_price() {
		WebElement we = driver.findElement(By.xpath("(//div[@class='product-item'])[1]"));
        System.out.println(we.getText());
	}

	@When("user have to logout the application")
	public void user_have_to_logout_the_application() {
		driver.findElement(By.linkText("Log out")).click();
	}

	@Then("login button should be displayed  on the homepage")
	public void login_button_should_be_displayed_on_the_homepage() {
		if (driver.findElement(By.linkText("Log in")).isDisplayed()) {
            System.out.println("login is displayed");
        } else {
            System.out.println("login is not displayed");
        }
		driver.quit();
	}
}
