Feature: TO validate DemoWebShop Application Functionality

Scenario Outline: To validate login functionality
Given user have to enter DemoWebShop application through chrome browser
When click on the login button
And user have to enter valid "<email>" and valid "<password>"
Then user have to click login button
And user have to get title of the home page
Examples:
|         email       | password |
|manzmehadi1@gmail.com| Mehek@110|

Scenario: To validate Add to cart for product functionality
Given user have to click book category
When user have to select price category
And user have to  sort price high to low
Then user have to Add books in add to cart
And user have to click Electronic category
And user have to select cellphone
Then user have to add any product in add to cart
And user have to display count of items present in shopping cart

Scenario: To validate Giftcard and  Logout functionality
Given user have to select gift card category
When user have to capture any one of the giftcard name and price
And user have to logout the application
Then login button should be displayed  on the homepage

